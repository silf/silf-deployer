Deployment scripts for SILF experiments
=======================================

Setup this repository
---------------------

1. Create virtual enviorment
2. Install ``sshpass`` program.
3. Install requirements
4. Get in touch with someone to get vault password and save it to ``experiment/.vault-key``
   (can be any other path, but examples will assume this path).

   ``.vault-key`` is also in secrets keyfile.

Login to experiment
-------------------

1. Root access is by ``ssh key`` only to get this access, add your public ssh key
   to ``experiment/data/admin_keys`` and ask someone to run ``init-provision-user.yaml``
   on all experiments.
2. There is shared password for experiment.

Add new key to all experiments
------------------------------

1. Add ssh key to ``data/admin_keys``.
2. ``ansible-playbook --vault-password-file .vault-key init-provision-user.yaml``

Add new experiment to the cluster
---------------------------------

#### Password for image

For stock image password is ``raspberry`` for our image password is in password file under "default raspberry password"

#### Prepare the raspberry


1. Download raspbiann wheezy image:
https://ocs-pl.oktawave.com/v1/AUTH_385fff76-290b-43da-b2fc-96b1c08bce24/tools/rpi-image-raspbian-2017-01-18.xz
or:
https://www.raspberrypi.org/downloads/raspbian/
2. Burn the image
3. Remove and insert the card (reloads partition table)
4. Resize the image to fit on the card
5. Obtain static IP address.
6. Insert sd card to your computer to configure ``/etc/network/interfaces`` as follows:

       auto eth0
       iface eth0 inet static
               address <ip address>
               netmask 255.255.254.0
               gateway 10.66.0.254
               dns-nameservers 8.8.8.8 8.8.4.4

7. Connect raspberry, verify you can ssh to it (using default raspberry password!).

#### Prepare SILF

Add experiment to production instance. Instructions are here:
https://bitbucket.org/silf/docker

#### Prepare this repository

1. Add new entry to ``hosts`` file, this entry will look like:
   ``<experiment_name> ansible_ssh_host=<experiment_ip>``
2. Create ``host_vars/<experiment_name>`` directory
3. Create ``private.yml`` and ``public.yml``. Variables you need to update are in
   ``group_vars/all/override-for-experiment.yml``, put them all in ``public.yml``,
   except for passwords that are in ``private.yml``.


#### One time raspberry setup

Create user to be used for provisioning:
``ansible-playbook --vault-password-file .vault-key -l <experiment_name> -u pi init-provision-user.yaml -kK``

This will login to raspberry as ``pi`` user using password authentication (default
password is ``raspberry``).

This will: disable ``pi`` account for remote login, add ``provision account``.

#### Vagrant test setup

**Only for test in vagrant environment**

1. Start virtualbox vm or vagrant image ``vagrant up``.
2. Connect to virtualbox ``vagrant ssh``
3. Allow to password authentication in ``/etc/ssh/sshd_config``
4. Set nameserver to 8.8.8.8 in ```/etc/resolv.conf```

Create user to be used for provisioning:
``ansible-playbook --vault-password-file .vault-key -l <experiment_name> -u vagrant init-provision-user.yaml -kK``


#### Deploy the experiment

Each experiment is a separate host in ansible configuration, to see list of experiments
yoy can re deploy just ``ls host_vars``. 

To re-deploy experiment named ``attenuation`` you'll have to: 

0. ``cd experiment``
1. ``ansible-playbook --vault-password-file .vault-key -l attenuation experiment-full.yaml``

Switch ``-l`` tells ansible to re-deploy only ``attenuation`` experiment. 


#### Cleanups

1. Make sure that all ``private.yml`` files are encrypted. To encrypt a file use:
   ``ansible-vault encrypt --vault-password-file .vault-key``
2. Squash all comits with unencrypted files.
3. Push

Re-deploy existing experiment
-----------------------------

Each experiment is a separate host in ansible configuration, to see list of experiments
yoy can re deploy just ``ls host_vars``.

To re-deploy experiment named ``attenuation`` you'll have to:

0. ``cd experiment``
1. ``ansible-playbook --vault-password-file .vault-key -l attenuation experiment-redeploy.yml``

Doing ``experiment-full.yaml`` wont work -- as we freeze apt due to problems with compiled extensions. 

Helpful note
------------

I did create ``source.me`` file in this repository containing:

    source venv/bin/activate
    alias av='ansible-vault --vault-password-file .vault-key '
    alias ap='ansible-playbook --vault-password-file .vault-key '

Which allow me to just ``ap experiment-full.yaml``.

Check experiment logs
---------------------

Login to raspberry and then: ``sudo journalctl -u experiment.unit``.
