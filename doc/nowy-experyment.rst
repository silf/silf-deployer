1. Prepare your computer
 a. Create virtual enviorment **using python 2.7**
 b. Install ``ssh-pass`` program.
 c. Install requirements
 d. Get in touch with someone to get vault password and save it to experiment/.vault-file
    (can be any other path, but examples will assume this path).
 e. Connect to VPN.

2. Prepare the repository
  a. Add your public key to ``experiment/data/admin_keys``.
  b. Add your experiment to ``experiment/hosts`` entry should look like::

     experiment_codename  ansible_ssh_host=10.66.1.XX
  c. Add required configuration to ``host_vars``, to see configuration for
     experiment use (when in ``experiment`` directory``::

        ansible-vault view --vault-password-file=.vault-file host_vars/attenuation.yaml

     To decrypt a file use ``decrypt`` to encrypt use ``encrypt``.

     Save configuration in ``host_vars/experiment_codename.yaml`` (experiment_codename
     must match your entry in hosts).

     Configuration looks like that::

        ---
        experiment_repo:
          repo_url: git@XXXX # GIT URL
          repo_version: master # GIT BRANCH

        pass:
          smtp: XXX # SMTP password --- does not vary between experiments
          xmpp: XXX # XMPP password


  d. Commit and push

3. Prepare the image
 a. Download raspbiann jessie image: https://www.raspberrypi.org/downloads/raspbian/
 b. Burn the image
 c. Remove and insert the card (reloads partition table)
 d. Resize the image to fit on the card
 e. Update /etc/networking/interfaces to match new IP

    This is the example configuration::

        auto lo
        iface lo inet loopback

        auto eth0
        allow-hotplug eth0
        iface eth0 inet static
           address 10.66.1.XXX
           netmask 255.255.254.0
           gateway 10.66.0.1

        auto wlan0
        allow-hotplug wlan0
        iface wlan0 inet manual
        wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

        auto wlan1
        allow-hotplug wlan1
        iface wlan1 inet manual
        wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf


4. Perform initial configuration, to do this you'll need to use command
   similar to::

    ansible-playbook init-provision-user.yaml --vault-password-file=.vault-file -Kk --user=pi -l experiment_codename

  * ``--user=pi`` will login remotely as ``pi`` user
  * ``-l experiment_codename`` will execute changes only to your experiments
  * ``-kK`` will ask you about login password and sudo password.
  This playbook creates a ``provision`` user that will be used by all other
  playooks.

  Default password is: ``raspberry``.

  .. note::

    At this time you might try to connect using plain SSH, to fix all problems
    with Host keys.


5. Perform full deployment::

    ansible-playbook experiment-full.yaml --vault-password-file=.vault-file -l experiment_codename

   .. note::

    If you use ``ssh-agent`` you need to add the privae key matching public key
    you added to ``admin_keys``.

    If you don't want to use ``ssh-agent`` you need to add ``--private-key``
    parameter to all your invocations.

6. Other playbooks:

    * ``experiment-redeploy.yaml`` --- just







X. Ask someone to pull your key and add your key to other experiments.

