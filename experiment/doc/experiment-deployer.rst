Experiment deployer howto
=========================

1. It uses ansible `vault <https://docs.ansible.com/ansible/playbooks_vault.html>`__.
   Password is **not in the repository** and Jacek knows it. Then put the password
   (for example) in ``.vault-file``.
2. For access to the raspberry your key needs to be added to the
   ``data/admin_keys``
3. You need to have ansible installed (see ``requirements.txt``)
4. We use (only) ssh-key based logins. Shared passwords sucks.

To setup on virgin card
-----------------------

1. Install the card on a target raspberry pi (card has to have rasbian
   image burned)
2. Set up the raspberry to use proper IP.
3. Following comands will set-up ``provision`` user (used for provisioning):
   ``ansible-playbook init-provision-user.yaml -l geiger  -u pi  --vault-password-file .vault-file --ask-pass -s``
4. Then use ``generic-experiment.yaml`` to set up experiment.
5. Ssh to the raspberry and restart it (needed to install i2c kernell stuff).

To update (update, then restart) all experiments
------------------------------------------------

1. Issue command: ``ansible-playbook generic-experiment.yaml --vault-password-file=.vault-file``.


Assumptions
-----------

* You use can use ``i2c``.
* In the main directory of git repository you have a working ``experiment.ini``
  file. Passwords (XMPP, SMTP) will be stored in ``password.ini`` and generated
  by ansible.
* All requirements are stored in ``REQUIREMENTS`` file. Virtualenv will have
  precompiled ``lxml`` installed.
* Python 3.5 is installed and used.
* For now raspberry pi does not use git. Repository is downloaded locally
  to **your** machine then syced by ssh.

How to add new experiment
-------------------------

In the (optimistic) assumption that everyting will work out of the box.

* Add experiment host to hosts file.
* Add ``host_vars/host_name.yaml`` file with contents similar to those in
  ``host_vars/attenuation.yaml``. To see it use: ``ansible-vault view host_vars/attenuation.yaml --vault-password-file .vault-file``.

Deployment structure
--------------------

* Experiment is installed under ``experiment`` user.
* Experiment repository is in ``/home/experiment/repo``, virtualenv is under
  ``/home/experiment/venv``
* To start experiment run ``start.sh`` script. This script is launched by
  supervisord.






