#!/usr/bin/env bash

cd /home/experiment/repo
source /home/experiment/venv/bin/activate
export PYTHONPATH=/home/experiment/repo
{% if experiment_configuration.languages %}
# Note if this fails double check experment translation domain
make-translations.py compile --experiment={{ experiment_configuration.experiment_translation_domain }} --locales={{ experiment_configuration.languages }}
{% endif %}
expmain.py {% if USE_INI_FILE_FROM_THE_REPOSITORY %}experiment.ini{% else %}{{ EXPERIMENT_INI_TARGET }}{% endif %}
